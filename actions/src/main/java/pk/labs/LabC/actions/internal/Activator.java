/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author globalbus
 */
public class Activator implements BundleActivator {

    private static BundleContext context;

    @Override
    public void start(BundleContext bc) {
        context = bc;
        AnimalAction animal1 = new AnimalAction() {
            public String toString() {
                return "Jedz";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("Jedz");
                return true;
            }
        };
        AnimalAction animal2 = new AnimalAction() {
            public String toString() {
                return "Pij";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("Pij");
                return true;
            }
        };
        AnimalAction animal3 = new AnimalAction() {
            public String toString() {
                return "Skacz";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("Skacz");
                return true;
            }
        };
        Hashtable test1 = new Hashtable();
        test1.put("species", "Jaguar");
        Hashtable test2 = new Hashtable();
        test2.put("species", new String[]{"Zajac"});
        Hashtable test3 = new Hashtable();
        test3.put("species", "Antylopa");
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), animal1, null);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), animal2, test2);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), animal3, null);
    }

    @Override
    public void stop(BundleContext bc) {
        context = null;
    }
}
