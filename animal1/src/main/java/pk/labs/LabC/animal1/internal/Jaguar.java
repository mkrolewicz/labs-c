/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal1.internal;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author Marcin
 */
public class Jaguar implements Animal {

    private String status;
    private static PropertyChangeSupport listeners;

    public Jaguar() {
        listeners = new PropertyChangeSupport(this);
    }

    @Override
    public String getSpecies() {
        return "Jaguar";
    }

    @Override
    public String getName() {
        return "Jag";
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        String oldValue=this.status;
        this.status=status;
        listeners.firePropertyChange( new PropertyChangeEvent(this, "status", oldValue, status));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(listener);
    }

}
