/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal3.internal;

/**
 *
 * @author Marcin
 */
import javax.naming.Context;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
/**
 *
 * @author Marcin
 */
public class Activator implements BundleActivator {

    private static BundleContext Context;
    
    @Override
    public void start(BundleContext bc) throws Exception {
        Context = bc;
        Context.registerService(Animal.class.getName(), new Antylopa(), null);
        pk.labs.LabC.logger.Logger.get().log(this, "Antylopa Biegnie");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        Context = null;
        pk.labs.LabC.logger.Logger.get().log(this, "Antylopa odbiega");
    }
    
}
